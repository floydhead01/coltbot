import os
import openai
import logging
import json
from dotenv import load_dotenv
import threading

# Import WebClient from Python SDK (github.com/slackapi/python-slack-sdk)
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError

from flask import Flask, redirect, url_for, request

load_dotenv()

openai.api_key = os.environ["OPENAPI_KEY"]
slack_key = os.environ["SLACK_KEY"]
bot_user = os.environ["BOT_USER"]

# WebClient instantiates a client that can call API methods
# When using Bolt, you can use either `app.client` or the `client` passed to listeners.
client = WebClient(token=slack_key)
logger = logging.getLogger(__name__)

#will prob lead to a memory issue one day
users = {}


def get_summery(text):
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=text + "\n\nTl;dr",
        temperature=0.7,
        max_tokens=100,
        top_p=1.0,
        frequency_penalty=0.0,
        presence_penalty=1
    )
    return response

def lookupuser(user):

    if user in users:
        return users[user]["real_name"]

    result = client.users_info(user=user)
    print (result)
    if "user" in result:
        users[user] = result["user"]
        return result["user"]["real_name"]

    return "NEMO"


def get_convo_summery(conversation_id, thread_id):
    #conversation_id = None
    try:
        print(conversation_id+'|'+(thread_id if thread_id else "No Thread"))
        conversation_history = []

        if thread_id is None:
            result = client.conversations_history(channel=conversation_id)
            conversation_history = result["messages"]
        else:
            result = client.conversations_replies(channel=conversation_id, ts=thread_id)
            conversation_history = result["messages"]

        history = []

        convo_text = ""
        for message in conversation_history:
            if message["user"] != bot_user:
                username = lookupuser(message["user"])
                msg = username + ":" + message["text"]
                history.append(msg)

        #flip conversation history
        history.reverse()

        convo_text = "\n".join([str(item) for item in history])

        print(convo_text)
        print("=====sumerizing========")
        summery = get_summery(convo_text)
        print(summery)
        return summery['choices'][0]["text"]

    except SlackApiError as e:
        print(f"Error: {e}")


# Threaded wrapper for request
def handle_mention_request(channel, thread = None):
    summery = get_convo_summery(channel, thread)
    result = client.chat_postMessage(
        channel=channel,
        text=summery,
        thread_ts=thread
        # You could also use a blocks[] array to send richer content
    )
    print(summery)


app = Flask(__name__)


## Handles slack callbacks, specfically challenge and app_mention
@app.route('/', methods=['POST'])
def process_request():

    request_json = request.get_json()

    print(request_json)

    if("event" in request_json and request_json["event"]["type"] == "app_mention"):
        channel = request_json["event"]["channel"]
        slack_thread = None
        if "thread_ts" in request_json["event"]:
            slack_thread = request_json["event"]["thread_ts"]

        # launch thread since calls take time, and slack expects quick response else spams you.
        thread = threading.Thread(target=handle_mention_request, kwargs={'channel': channel, 'thread': slack_thread})
        thread.start()
        
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 

    return request_json["challenge"]

@app.route('/', methods=['GET'])
def healthcheck():
    return 'OK'

if __name__ == '__main__':
   app.run(debug = True)
#print (get_summery(""))